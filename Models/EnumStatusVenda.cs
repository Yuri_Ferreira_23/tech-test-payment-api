namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        Aguadando_Pagamento,
        Pagamento_Aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelado
    
    }
}