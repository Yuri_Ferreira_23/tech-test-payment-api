using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedorController : ControllerBase
    {
         private readonly PaymentApiContext _context;
        public VendedorController(PaymentApiContext context)
        {
            _context = context;
        }

        [HttpPost("CriarVendedor")]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = vendedor.Id }, vendedor);  
        }

        [HttpGet("ObterVendedorPorId")]
        public IActionResult ObterVendedorPorId(int id)
        {
            var vendedor = _context.Vendedors.Find(id);

            if(vendedor == null){
                return NotFound();
            }

            return Ok(vendedor);
        }

        [HttpGet("ObterTodosVendedores")]
        public IActionResult ObterVendedores()
        {
            var vendedor = _context.Vendedors;
            return Ok(vendedor);
        }

        [HttpPut("AtualizarVendedor")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedors.Find(id);
            
            if (vendedorBanco == null){
                return NotFound();
            }

            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;

            _context.Vendedors.Update(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);
        }

        [HttpDelete("RemoverVendedor")]
        public IActionResult Deletar(int id)
        {
            var vendedor = _context.Vendedors.Find(id);

            if (vendedor == null){
                return NotFound();
            }
            _context.Vendedors.Remove(vendedor);
            _context.SaveChanges();
            return NoContent();
        }
    }
}