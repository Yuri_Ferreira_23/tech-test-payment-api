using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly PaymentApiContext _context;
        public ItemController(PaymentApiContext context)
        {
            _context = context;
        }

        [HttpPost("CriarItem")]
        public IActionResult Create(Item item)
        {
            _context.Add(item);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterItemPorId), new { id = item.Id }, item);
        }
        
        [HttpGet("ObterItens")]
        public IActionResult ObterItens()
        {
            var item = _context.Items;
            return Ok(item);
        }


        [HttpGet("ObterItemPorId")]
        public IActionResult ObterItemPorId(int id)
        {
            var item = _context.Items.Find(id);

            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpPut("AtualizarItem")]
        public IActionResult Atualizar(int id, Item item)
            {
                var itemBanco = _context.Items.Find(id);
                
                if(itemBanco == null){
                    return NotFound();
                }
                itemBanco.Descricao = item.Descricao;
                itemBanco.Valor = item.Valor;

                _context.Items.Update(itemBanco);
                _context.SaveChanges();

                return Ok(itemBanco);
            }

        [HttpDelete("RemoverItem")]
        public IActionResult Deletar(int id)
            {
                var itemBanco = _context.Items.Find(id);
                
                if(itemBanco == null){
                    return NotFound();
                }
                _context.Items.Remove(itemBanco);
                _context.SaveChanges();

                return NoContent();
            }
    }
}